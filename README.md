## About Assessment

Used Laravel, Full Stack Web Application Framework

Only Authenticated users can able to access the application

### User Access
## Admin
Admin can access Users and Loans Module
Able to approve the loans and verify the loan listing of all the users

## Non-Admin(Users)
Able to Request Loan
Able to Repay the loan from detail page and verify all the listing for the same user only

Anyone can able to register as user and access the application for now

## Admin Credentials:
Username/Email: email@aspire.com
Password: Aspire

Have added the screenshots of the application with this

Achieved the application with the help of HTML, Bootstrap, jQuery, PHP-Laravel


## Instructions to install
* git clone {repo-name}
* composer install
* chmod -R 777 storage/ bootstrap/
* php artisan migrate
* php artisan db:seed

Now we can get the application same as screenshots
