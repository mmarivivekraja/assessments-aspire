<?php

namespace App\Service;

use App\Models\Loan;
use App\Models\LoanMeta;
use App\Repository\Contract\LoanRepositoryInterface;
use App\Service\Contract\LoanServiceInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LoanService extends BaseService implements LoanServiceInterface
{
    protected $repository;

    const DEFAULT_INTEREST_RATE = 3;

    public function __construct(LoanRepositoryInterface $repository)
    {
        parent::__construct($repository);
        $this->repository = $repository;
    }

    public function getByUserId(int $userId)
    {
        return $this->repository->where(['user_id' => $userId])->paginate();
    }

    public function store(array $params)
    {
        $tenureAmount = ($params['total_amount'] / $params['total_tenure']);
        $tenureAmountWithInterest = $tenureAmount + ($tenureAmount * (self::DEFAULT_INTEREST_RATE / 100));
        $params['installment_amount'] = $tenureAmountWithInterest;
        Loan::create($params);
    }

    public function approve(int $loanId)
    {
        $loan = $this->repository->find($loanId);
        $loan->approved_by = Auth::id();
        $loan->approved_at = Carbon::now();
        $loan->save();
    }

    public function getDebtors()
    {
        return Loan::groupBy('user_id')->get();
    }

    public function payInstallment(int $loanId)
    {
        LoanMeta::create([
            'loan_id' => $loanId,
            'paid_at' => Carbon::now()
        ]);
    }
}