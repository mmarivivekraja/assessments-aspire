<?php

namespace App\Service;

use App\Repository\Contract\UserRepositoryInterface;
use App\Service\Contract\UserServiceInterface;

class UserService extends BaseService implements UserServiceInterface
{
    public function __construct(UserRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }
}