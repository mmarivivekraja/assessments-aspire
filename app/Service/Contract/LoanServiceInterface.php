<?php

namespace App\Service\Contract;

use Illuminate\Database\Eloquent\Collection;

interface LoanServiceInterface extends ServiceInterface
{
    public function getByUserId(int $userId);
    
    public function store(array $params);

    public function approve(int $loanId);

    public function getDebtors();
    
    public function payInstallment(int $loanId);
}