<?php

namespace App\Service\Contract;

use Illuminate\Support\Collection;

interface ServiceInterface
{
    public function all(array $columns = ['*']) : Collection;

    public function find(int $id, array $columns = ['*']);

    public function findOrFail(int $id);

    public function search(string $searchString, array $columns, array $sorts, int $perPage);

    public function where(array $where, array $columns = ['*']);

    public function whereIn(string $field, array $values);
}
