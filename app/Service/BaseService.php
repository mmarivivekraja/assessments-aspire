<?php

namespace App\Service;

use App\Repository\Contract\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class BaseService
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Retrieve all data of repository.
     *
     * @param array $columns
     * @return mixed
     */
    public function all(array $columns = ['*']): Collection
    {
        return $this->repository->all($columns);
    }

    /**
     * Find data by id.
     *
     * @param $id
     * @param array $columns
     * @return Model | null
     */
    public function find(int $id, array $columns = ['*'])
    {
        return $this->repository->find($id, $columns);
    }

    public function allWithPagination(
        int $perPage = null,
        array $columns = ['*'],
        string $pageName = 'page',
        int $page = null,
        array $sorts = []
    ) {
        return $this->repository->paginate($perPage, $columns, $pageName, $page, $sorts);
    }

    public function search(string $searchString, array $columns, array $sorts, int $perPage)
    {
        return $this->repository->search($searchString, $columns, $sorts, $perPage);
    }

    public function where(array $where, array $columns = ['*'])
    {
        return $this->repository->where($where, $columns);
    }

    public function findOrFail(int $id)
    {
        return $this->repository->findOrFail($id);
    }

    public function whereIn(string $field, array $values)
    {
        return $this->repository->whereIn($field, $values);
    }
}