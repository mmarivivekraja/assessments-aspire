<?php

namespace App\Enum;

class Status
{
    const ADMIN = 1;
    const NON_ADMIN = 2;
}