<?php

namespace App\Providers;

use App\Repository\Contract\LoanRepositoryInterface;
use App\Repository\Contract\LoanRepositoryServiceInterface;
use App\Repository\Contract\UserRepositoryInterface;
use App\Repository\Contract\UserRepositoryServiceInterface;
use App\Repository\LoanRepository;
use App\Repository\LoanRepositoryService;
use App\Repository\UserRepository;
use App\Repository\UserRepositoryService;
use App\Service\Contract\LoanServiceInterface;
use App\Service\Contract\UserServiceInterface;
use App\Service\LoanService;
use App\Service\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserServiceInterface::class, UserService::class);
        $this->app->singleton(UserRepositoryInterface::class, UserRepository::class);

        $this->app->bind(LoanServiceInterface::class, LoanService::class);
        $this->app->bind(LoanRepositoryInterface::class, LoanRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
