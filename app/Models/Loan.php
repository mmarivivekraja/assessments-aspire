<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loan extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'total_amount',
        'total_tenure',
        'user_id',
        'purpose',
        'frequency',
        'installment_amount',
    ];

    public function balanceAmount()
    {
        return number_format($this->total_amount - ($this->meta->count() * $this->installment_amount), 2);
    }

    public function meta()
    {
        return $this->hasMany(LoanMeta::class, 'loan_id', 'id');
    }

    public function balanceTenure()
    {
        return $this->total_tenure - $this->meta->count();
    }

    public function getApproverName()
    {
        return $this->approvedBy ? $this->approvedBy->name : '';
    }

    public function getDebtorName()
    {
        return $this->user ? $this->user->name : '';
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function approvedBy()
    {
        return $this->hasOne(User::class, 'id', 'approved_by');
    }
}
