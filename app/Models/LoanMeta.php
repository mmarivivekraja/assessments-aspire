<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanMeta extends Model
{
    use HasFactory;

    protected $table = 'loan_meta';

    public $timestamps = false;

    protected $fillable = [
        'loan_id',
        'paid_at'
    ];

    public $dates = [
        'paid_at'
    ];
}
