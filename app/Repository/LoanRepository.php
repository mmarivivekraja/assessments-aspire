<?php

namespace App\Repository;

use App\Models\Loan;
use App\Repository\Contract\LoanRepositoryInterface;

class LoanRepository extends BaseRepository implements LoanRepositoryInterface
{
    public function model(): string
    {
        return Loan::class;
    }

    public function create($data): Loan
    {
        return $this->model->create($data);
    }

    public function update(array $data, int $categoryId): Loan
    {
        $this->model->where('id', $categoryId)->update($data);

        return $this->get($categoryId);
    }

    public function get(int $categoryId): Loan
    {
        return $this->model->find($categoryId);
    }

    public function delete(int $categoryId)
    {
        return $this->model->destroy($categoryId);
    }
}