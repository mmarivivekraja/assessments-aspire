<?php

namespace App\Repository;

use App\Models\User;
use App\Repository\Contract\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function model(): string
    {
        return User::class;
    }

    public function create($data): User
    {
        return $this->model->create($data);
    }

    public function update(array $data, int $categoryId): User
    {
        $this->model->where('id', $categoryId)->update($data);

        return $this->get($categoryId);
    }

    public function get(int $categoryId): User
    {
        return $this->model->find($categoryId);
    }

    public function delete(int $categoryId)
    {
        return $this->model->destroy($categoryId);
    }
}