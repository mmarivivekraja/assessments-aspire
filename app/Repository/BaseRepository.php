<?php

namespace App\Repository;

use App\Repository\Contract\RepositoryInterface;
use Illuminate\Container\Container as Application;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

abstract class BaseRepository implements RepositoryInterface
{
    /** @var App */
    private $app;

    /** @var Model */
    protected $model;

    abstract public function model(): string;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    public function makeModel()
    {
        $model = $this->app->make($this->model());

        return $this->model = $model;
    }

    /**
     * Retrieve all data of repository.
     *
     * @param array $columns
     * @return mixed
     */
    public function all(array $columns = ['*'])
    {
        return $this->model->all($columns);
    }

    /**
     * {@inheritdoc}
     */
    public function select(array $columns = ['*']): Builder
    {
        return $this->model->select($columns);
    }

    /**
     * Find data by id.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = ['*'])
    {
        return $this->model->find($id, $columns);
    }

    /**
     * Find all data by multiple fields.
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function where(array $where, array $columns = ['*'])
    {
        return $this->model->where($where, $columns);
    }

    /**
     * Paginate the given query.
     *
     * @param  int $perPage
     * @param  array $columns
     * @param  string $pageName
     * @param  int|null $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null, array $sorts = [])
    {
        $query = $this->model->newQuery();

        foreach ($sorts as $fieldName => $order) {
            $query->orderBy($fieldName, $order);
        }

        return $query->paginate($perPage, $columns, $pageName, $page);
    }

    /**
     * {@inheritdoc}
     */
    public function destroy(int $id)
    {
        return $this->model->destroy($id);
    }

    public function search(string $searchString, array $columns, array $sorts, int $perPage)
    {
        if (empty($columns)) {
            throw new InvalidArgumentException('No fields to search into.');
        }

        $query = $this->model->newQuery();

        foreach ($columns as $fieldName => $operator) {
            switch (strtolower($operator)) {
                case 'like':
                    $query->orWhere($fieldName, $operator, '%' . $searchString . '%');
                    break;
            }
        }

        foreach ($sorts as $fieldName => $order) {
            $query->orderBy($fieldName, $order);
        }

        return $query->paginate($perPage, ['*'], config('project.pagination_query_parameter'), 1);
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function whereIn(string $field, array $values)
    {
        return $this->model->whereIn($field, $values);
    }

    public function save(Model $model): bool
    {
        return $model->save();
    }
}