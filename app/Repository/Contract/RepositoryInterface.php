<?php

namespace App\Repository\Contract;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * Retrieve all data of repository.
     *
     * @param array $columns
     * @return mixed
     */
    public function all(array $columns = ['*']);

    /**
     * Return an Eloquent Builder.
     */
    public function select(array $columns = ['*']): Builder;

    /**
     * Find data by id.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = ['*']);

    /**
     * Find all data by multiple fields.
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function where(array $where, array $columns = ['*']);

    /** @throws \InvalidArgumentException */
    public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null, array $sorts = []);

    /** Destroy by id */
    public function destroy(int $id);

    public function search(string $searchString, array $columns, array $sorts, int $perPage);

    public function findOrFail($id);

    public function whereIn(string $field, array $values);

    public function save(Model $model): bool;
}
