<?php

namespace App\Http\Controllers;

use App\Enum\Status;
use App\Http\Requests\LoanRequest;
use App\Models\Loan;
use App\Service\Contract\LoanServiceInterface;
use App\Service\Contract\UserServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    protected $userService;

    protected $loanService;

    /**
     * LoanController constructor.
     * @param UserServiceInterface $userServiceInterface
     * @param LoanServiceInterface $loanServiceInterface
     */
    public function __construct(UserServiceInterface $userServiceInterface, LoanServiceInterface $loanServiceInterface)
    {
        $this->userService = $userServiceInterface;
        $this->loanService = $loanServiceInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $temp = 1;
        if(Auth::user()->is_admin == Status::ADMIN) {
            $loans = $this->loanService->getDebtors();
            return view('loans.admin-listing', compact('loans', 'temp'));
        }

        $loans = $this->loanService->getByUserId(Auth::id());
        return view('loans.listing', compact('loans', 'temp'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->is_admin == Status::ADMIN) {
            abort(404);
        }

        return view('loans.create');
    }

    /**
     * @param LoanRequest $request
     * @return mixed
     */
    public function store(LoanRequest $request)
    {
        if(Auth::user()->is_admin == Status::ADMIN) {
            abort(404);
        }

        $this->loanService->store(array_merge($request->except(['_token']), ['user_id' => Auth::id()]));
        return redirect('loans')->with('status', 'You have successfully requested for Loan');
    }


    public function debtorDetails(Request $request, int $userId)
    {
        $temp = 1;
        $user = $this->userService->find($userId);
        return view('loans.debtor-details', compact('user', 'temp'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
        return view('loans.show', compact('loan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $loan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        //
    }

    /**
     * @param Request $request
     * @param int $loanId
     * @return mixed
     */
    public function approve(Request $request, int $loanId)
    {
        $this->loanService->approve($loanId);
        return redirect('loans')->with('status', 'Loan has been approved!');
    }

    public function payInstallment(Request $request, int $loanId)
    {
        $this->loanService->payInstallment($loanId);
        return redirect('loans')->with('status', 'Installment amount has been paid!');
    }
}
