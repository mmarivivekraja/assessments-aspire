<?php

namespace App\Http\Controllers;

use App\Enum\Status;
use App\Service\Contract\UserServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userServiceInterface;

    /**
     * UserController constructor.
     * @param UserServiceInterface $userServiceInterface
     */
    public function __construct(UserServiceInterface $userServiceInterface)
    {
        $this->userServiceInterface = $userServiceInterface;
    }

    public function index(Request $request)
    {
        if (Auth::user()->is_admin == Status::NON_ADMIN) {
           abort(404);
        }

        $users = $this->userServiceInterface->all();
        $temp = 1;
        return view('users.listing', compact('users', 'temp'));
    }
}
