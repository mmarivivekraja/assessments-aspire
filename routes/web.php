<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});


Route::group([
    'middleware' => [],
], function () {
    //Basic authorization
    Route::group(['middleware' => 'auth'], function () {

        Route::resource('users', \App\Http\Controllers\UserController::class);
        Route::resource('loans', \App\Http\Controllers\LoanController::class);
        Route::get('loans/{loanId}/approve', '\App\Http\Controllers\LoanController@approve');
        Route::get('loans/{loanId}/pay-now', '\App\Http\Controllers\LoanController@payInstallment');
        Route::get('loans/user/{userId}', '\App\Http\Controllers\LoanController@debtorDetails');
    });

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

});

Auth::routes();

