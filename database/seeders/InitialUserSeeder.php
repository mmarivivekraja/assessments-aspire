<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class InitialUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Aspire Admin',
            'email' => 'email@aspire.com',
            'is_admin' => 1,
            'password' => bcrypt('Aspire'),
        ]);
    }
}
