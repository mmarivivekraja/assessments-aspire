<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->string('purpose');
            $table->string('frequency', 10)->default('Weekly');
            $table->decimal('total_amount', 12, 2);
            $table->integer('total_tenure');
            $table->integer('interest_rate')->default(3)->comment("Rate in percentage");
            $table->decimal('installment_amount')->default(0.00);
            $table->integer('user_id');
            $table->integer('approved_by')->nullable();
            $table->dateTime('approved_at')->nullable();
            /*$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('approved_by')->references('id')->on('users')->onDelete('cascade');*/
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
