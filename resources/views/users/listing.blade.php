
@extends('layouts.app')

@section('content')
<div class="content-wrapper" style="min-height: 163px; margin-left: 0px !important">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listing</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                   <div class="row">
                   </div>
                   <div class="row">
                      <div class="col-sm-12">
                         <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                            <thead>
                               <tr role="row">
                                   <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">#ID</th>
                                   <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Name</th>
                                  <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Email</th>
                                   <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Is Account Verified</th>
                                   <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Is Admin</th>
                                   <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Created At</th>
                               </tr>
                            </thead>
                            <tbody>
                               @foreach($users as $user)
                                 <tr role="row" @if($temp % 2 == 0) class="even" @else class="odd" @endif>
                                    <td tabindex="0" class="sorting_1">{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                     <td>{{$user->email}}</td>
                                     <td>{{$user->email_verified_at ? 'Yes' : 'No' }}</td>
                                     <td>{{$user->is_admin == \App\Enum\Status::ADMIN ? 'Yes' : 'No'}}</td>
                                     <td>{{\Carbon\Carbon::parse($user->created_at, 'Y-m-d H:i:s')->diffForhumans()}}</td>
                                 </tr>
                                @endforeach
                            </tbody>
                         </table>
                      </div>
                      <div class="">
                        @if($users->count() == 0)
                          No users found.
                        @endif
                      </div>
                   </div>
                </div>
                </div>
              </div>
                
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
