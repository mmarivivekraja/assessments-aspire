@extends('layouts.app')
@section('content')
   <div class="col-md-12">
      <div class="card card-warning">
         <div class="card-header">
            <h3 class="card-title">Request Loan</h3>
         </div>
         <!-- /.card-header -->
         <div class="card-body">
            @if ($errors->any())
               <div class="alert alert-danger">
                  <ul>
                     @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                     @endforeach
                  </ul>
               </div>
            @endif
            <form role="form" method="POST" name="" action="{{ route('loans.store') }}">
               {{ csrf_field() }}
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group" id="category">
                        <label for="category">Loan Term</label>
                        <select class="form-control select2" required style="width: 100%;" tabindex="1" name="frequency">
                           <option value="Weekly">Weekly</option>
                        </select>
                     </div>
                     <div class="form-group" id="device">
                        <label for="device">Purpose</label>
                        <input type="text" class="form-control" required tabindex="2" name="purpose" id="purpose" value="{{ old('purpose') ? old('purpose') : '' }}" />
                     </div>
                     <div class="form-group">
                        <label>Amount</label>
                        <input type="number" class="form-control" required tabindex="3" name="total_amount" value="{{ old('total_amount') ? old('total_amount') : '' }}" />
                     </div>
                     <div class="form-group">
                        <label>Tenure</label>
                        <select class="form-control" name="total_tenure" id="total_tenure" required tabindex="4">
                           <option value="8">8 Weeks</option>
                           <option value="16">16 Weeks</option>
                           <option value="20">20 Weeks</option>
                           <option value="28">28 Weeks</option>
                           <option value="40">40 Weeks</option>
                        </select>
                     </div>

                     <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                     </div>
                  </div>
                  <div class="col-sm-6">
                  </div>
            </form>
         </div>
         <!-- /.card-body -->
      </div>
      <!-- /.card -->
   </div>
@endsection