
@extends('layouts.app')

@section('content')
<div class="content-wrapper" style="min-height: 163px; margin-left: 0px !important">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Loans</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/loans/create') }}">Request Loan</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listing</h3>
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                   <div class="row">
                   </div>
                   <div class="row">
                      <div class="col-sm-12">
                         <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                            <thead>
                               <tr role="row">
                                  <th>Purpose</th>
                                   <th>Total Amount</th>
                                   <th>Balance Amount</th>
                                   <th>Total Tenure</th>
                                   <th>Balance Tenure</th>
                                   <th>Approved By</th>
                                   <th>Approved At</th>
                               </tr>
                            </thead>
                            <tbody>
                               @foreach($loans as $loan)
                                 <tr role="row" @if($temp % 2 == 0) class="even" @else class="odd" @endif>
                                    <td tabindex="0" class="sorting_1"><a href="{{url('loans/'.$loan->id)}}">{{$loan->purpose}}</a></td>
                                     <td>$ {{$loan->total_amount ?? 0}}</td>
                                     <td>$ {{$loan->balanceAmount()}}</td>
                                     <td>{{$loan->total_tenure ?? 0}}</td>
                                     <td>{{$loan->balanceTenure()}}</td>
                                     <td>{{$loan->getApproverName()}}</td>
                                     <td>{{ $loan->approved_at ? \Carbon\Carbon::parse($loan->approved_at)->diffForhumans() : ''}}</td>
                                 </tr>
                                @endforeach
                            </tbody>
                         </table>
                      </div>
                      <div class="">
                        @if($loans->count() == 0)
                          No loans found. <a href="{{ route('loans.create') }}">Request Loan</a>
                        @endif
                      </div>
                   </div>
                </div>
                </div>
              </div>
                
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
