
@extends('layouts.app')

@section('content')
<div class="content-wrapper" style="min-height: 163px; margin-left: 0px !important">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Loans</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listing</h3>
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                   <div class="row">
                   </div>
                   <div class="row">
                      <div class="col-sm-12">
                         <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                            <thead>
                               <tr role="row">
                                   <th>Debtor Name</th>
                                   <th>Purpose</th>
                                   <th>Total Amount</th>
                                   <th>Balance Amount</th>
                                   <th>Total Tenure</th>
                                   <th>Balance Tenure</th>
                                   <th>Approved By</th>
                               </tr>
                            </thead>
                            <tbody>
                               @foreach($loans as $loan)
                                 <tr role="row" @if($temp % 2 == 0) class="even" @else class="odd" @endif>
                                     <td tabindex="0" class="sorting_1"><a href="{{url('loans/user/'.$loan->user_id)}}">{{$loan->getDebtorName()}}</a></td>
                                     <td>{{$loan->purpose}}</td>
                                     <td>$ {{$loan->total_amount ?? 0}}</td>
                                     <td>$ {{$loan->balanceAmount()}}</td>
                                     <td>{{$loan->total_tenure ?? 0}}</td>
                                     <td>{{$loan->balanceTenure()}}</td>
                                     <td>
                                        @if($loan->approved_by)
                                            {{$loan->getApproverName()}}
                                         @else
                                            <a href="{{url('loans/'.$loan->id.'/approve')}}">Approve</a>
                                         @endif
                                     </td>
                                 </tr>
                                @endforeach
                            </tbody>
                         </table>
                      </div>
                      <div class="">
                        @if($loans->count() == 0)
                          No loans found.
                        @endif
                      </div>
                   </div>
                </div>
                </div>
              </div>
                
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
