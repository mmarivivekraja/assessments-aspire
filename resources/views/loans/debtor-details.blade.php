
@extends('layouts.app')

@section('content')
<div class="content-wrapper" style="min-height: 163px; margin-left: 0px !important">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><a href="{{url('loans')}}"><i class="fa fa-arrow-left"></i> </a> #{{$user->id}} - {{$user->name}} </h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
              @foreach($user->loan as $loan)
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title">
                        Purpose: <b>{{$loan->purpose}}</b>
                    </h4><br/>
                      <div class="row invoice-info">
                          <div class="col-sm-4 invoice-col">
                              <address>
                                  Total Amount: <b>$ {{$loan->total_amount}}</b> <br>
                                  Total Tenure: <b>$ {{$loan->total_amount}}</b><br>
                                  Interest Rate: <b>{{$loan->interest_rate}} %</b><br>
                                  Installment Amount: <b>$ {{$loan->installment_amount}}</b>
                              </address>
                          </div>
                          <!-- /.col -->
                          <div class="col-sm-4 invoice-col">
                              <address>
                                  <strong></strong><br>
                                  Pending Amount: <b>$ {{$loan->balanceAmount()}}</b><br/>
                                  Pending Tenure: <b>{{$loan->balanceTenure()}}</b> <br/>
                              </address>
                          </div>
                          <!-- /.col -->
                          <div class="col-sm-4 invoice-col">
                              <br>
                              Approved By: <b>{{$loan->getApproverName()}}</b> <br>
                              Loan Requested On: <b>{{\Carbon\Carbon::parse($loan->created_at)->diffForHumans()}}</b> <br>
                          </div>
                          <!-- /.col -->
                      </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                       <div class="row">
                       </div>
@if($loan->meta)
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                                    <thead>
                                    <tr role="row">
                                        <th>Tenure No</th>
                                        <th>Paid At</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $tenureCount=1; @endphp
                                    @foreach($loan->meta as $meta)
                                        <tr role="row">
                                            <td>#{{$tenureCount}}</td>
                                            <td>{{ $meta->paid_at ? \Carbon\Carbon::parse($meta->paid_at)->diffForhumans() : ''}}</td>
                                        </tr>
                                        @php $tenureCount++; @endphp
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="">

                            </div>
                        </div>
    @endif
                  </div>

                  <!-- /.card-body -->
                </div>
                <!-- /.card -->


                <!-- /.card -->
              </div>
          @endforeach
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
